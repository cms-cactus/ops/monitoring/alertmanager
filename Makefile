SHELL:=/bin/bash
.DEFAULT_GOAL := help

#
# main variables
#

PROJECT_NAME=alertmanager
VERSION ?= $(shell git describe --always)
ALERTMANAGER_VERSION ?= 0.26.0
ARCH=amd64
RPM_NAME = cactus-alertmanager-${VERSION}-${RELEASE}.${ARCH}.rpm

#
# main targets
#

.PHONY: rpm
rpm: ${RPM_NAME} ## Builds RPM

.PHONY: clean
clean:
	rm -rf rpms rpmroot alertmanager-*.linux-${ARCH} cactus-alertmanager.yml


TESTFILES:=$(wildcard tests/*.json)
.PHONY: test $(TESTFILES)
test:  $(TESTFILES) ## Run test alerts on a running alertmanager instance

$(TESTFILES):
	./tests/alert_test.sh $@

#
# dependencies
#


ALERTMANAGER_STR = alertmanager-${ALERTMANAGER_VERSION}.linux-${ARCH}
${ALERTMANAGER_STR}:
	curl -LO https://github.com/prometheus/alertmanager/releases/download/v${ALERTMANAGER_VERSION}/${ALERTMANAGER_STR}.tar.gz
	tar xf ${ALERTMANAGER_STR}.tar.gz
	rm -rf ${ALERTMANAGER_STR}.tar.gz
	touch  ${ALERTMANAGER_STR}

.PHONY: setup
setup: ${ALERTMANAGER_STR} ## Downloads alertmanager

.PHONY: build
build: cactus-alertmanager.yml

cactus-alertmanager.yml: cactus-alertmanager-template.yml
ifndef SLACK_API_URL
		$(error SLACK_API_URL is not set)
endif
ifndef CACTUSBOT_PASSWORD
		$(error CACTUSBOT_PASSWORD is not set)
endif
	cat cactus-alertmanager-template.yml | envsubst > cactus-alertmanager.yml


${RPM_NAME}: ${ALERTMANAGER_STR} *
	rm -rf rpmroot
	mkdir -p rpmroot/opt/cactus/bin/alertmanager rpmroot/usr/lib/systemd/system rpmroot/opt/cactus/etc/alertmanager rpmroot/opt/cactus/etc/default/
	cp systemd/* rpmroot/usr/lib/systemd/system/
	cp ${ALERTMANAGER_STR}/alertmanager rpmroot/opt/cactus/bin/alertmanager/
	cp cactus-alertmanager-template.yml rpmroot/opt/cactus/etc/alertmanager/
	cp default/cactus-alertmanager.default rpmroot/opt/cactus/etc/default/cactus-alertmanager

	mkdir -p rpms
	cd rpmroot && fpm \
	-s dir \
	-t rpm \
	-n cactus-${PROJECT_NAME} \
	-v ${VERSION} \
	-a ${ARCH} \
	-m "<cactus@cern.ch>" \
	--vendor CERN \
	--description "alertmanager for the L1 Online Software at P5" \
	--url "https://gitlab.cern.ch/cms-cactus/ops/monitoring/alertmanager" \
	--provides cactus_$(subst -,_,${PROJECT_NAME}) \
	.=/ && mv *.rpm ../rpms/

.PHONY: help
help: ## Displays this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'