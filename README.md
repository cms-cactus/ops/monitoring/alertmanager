# Cactus Alertmanager

## Getting started

The Makefile contains the specification of which Alertmanager version will be downloaded.
Run ```make setup``` to automatically download and extract the corresponding alertmanager version.

### Configuration

The alertmanager depends on two secrets:

* the API location of the mattermost chat
* the cactusbot account password

Those are stored in the ```/nfshome0/centraltspro/secure/cactus-alertmanager/secrets``` folder in ```l1ts-prometheus``` . 
The actual running configuration of cactus AM is built starting from ```cactus-alertmanager-template.yml``` in which bash variables representing the two secrets are replaced by the real value at service startup.

In order to start up AM in a test environment, the cactusbot account password and the API path can be stored in two files, e.g. ```cactusbot_password.txt``` and ```mattermost_api.txt```.
Their content can then be assigned to the environment variables AM requires:

```bash
export SLACK_API_URL=`cat mattermost_api.txt`
export CACTUSBOT_PASSWORD=`cat cactusbot_password.txt`
```

Once set up, the alertmanager configuration can be built by running ```make build```.

## Running

Run with:

```bash
./alertmanager-INSERT_VERSION_HERE-linux-amd64/alertmanager --config.file cactus-alertmanager.yml
```

Make sure to remove the CMS proxy entry in the configuration to test notifications.

## Testing

Alerts can be tested by running ```make test```. Specific alerts can be tested by running ```make tests/<TEST NAME>.json```.
The test sends an alert notification via JSON file to alertmanager, which then fires alerts.