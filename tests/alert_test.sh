#!/usr/bin/env bash

json=$(cat $1)

curl --fail -H "Content-Type: application/json" -d "${json}" localhost:9093/api/v2/alerts